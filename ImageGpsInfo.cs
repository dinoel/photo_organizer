﻿using ExifLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
    public class ImageGpsInfo : IIMageGpsInfo, IDisposable
    {
        private ExifReader reader;

        public ImageGpsInfo(Stream imageStream)
        {
            this.reader = new ExifReader(imageStream);
        }

        public double[] GPSLongitude
        {
            get
            {
                double[] GPSLongitude = null;
                reader.GetTagValue(ExifTags.GPSLongitude, out GPSLongitude);
                return GPSLongitude;
            }
            private set
            {
            }
        }

        public double[] GPSLatitude
        {
            get
            {
                double[] GPSLatitude = null;
                reader.GetTagValue(ExifTags.GPSLatitude, out GPSLatitude);
                return GPSLatitude;
            }
            private set
            {
            }
        }

        public string GPSLatitudeRef
        {
            get
            {
                string GPSLatitudeRef = null;

                reader.GetTagValue(ExifTags.GPSLatitudeRef, out GPSLatitudeRef);
                return GPSLatitudeRef;
            }
            private set
            {

            }
        }

        public string GPSLongitudeRef
        {
            get
            {
                string GPSLongitudeRef = null;

                reader.GetTagValue(ExifTags.GPSLongitudeRef, out GPSLongitudeRef);
                return GPSLongitudeRef;
            }
            private set
            {

            }
        }

        public void Dispose()
        {
            this.reader.Dispose();
        }
    }

}
