﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
    public interface IIMageGpsInfo
    {
        double[] GPSLongitude { get; }
        double[] GPSLatitude { get; }
        string GPSLatitudeRef { get; }
        string GPSLongitudeRef { get; }
    }
}
