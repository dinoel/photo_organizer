﻿using System.Device.Location;
using System.IO;
using System.Text;

namespace WpfApplication2
{
    public class PictureLocationExtractor
    {
        private ReverseGeoCachingProvider geoCachingProvider;

        public PictureLocationExtractor(ReverseGeoCachingProvider geoCachingProvider)
        {
            this.geoCachingProvider = geoCachingProvider;
        }

        public string GetPictureLocation(FileInfo fileInfo)
        {
            var coordinate = GetFileCoordinates(fileInfo);
            string location = "No Address Found";
            if (coordinate != GeoCoordinate.Unknown)
            {
                location = this.geoCachingProvider.GetFormatedAddress(coordinate).Result;
            }

            return location;
        }

        private GeoCoordinate GetFileCoordinates(FileInfo fileInfo)
        {
            using (var fileStream = new FileStream(fileInfo.FullName, FileMode.Open))
            {
                if (!HasJpegHeader(fileStream))
                {
                    return GeoCoordinate.Unknown;
                }

                fileStream.Seek(0, SeekOrigin.Begin);

                using (var imageGPSInfo = new ImageGpsInfo(fileStream))
                {
                    return GeoCoordinatesConverter.Convert(imageGPSInfo);
                }
            }
        }

        private bool HasJpegHeader(Stream fileStream)
        {
            using (BinaryReader br = new BinaryReader(fileStream, new UTF8Encoding(), true))
            {
                ushort soi = br.ReadUInt16();  // Start of Image (SOI) marker (FFD8)
                ushort marker = br.ReadUInt16(); // JFIF marker (FFE0) or EXIF marker(FF01)

                return soi == 0xd8ff && (marker & 0xe0ff) == 0xe0ff;
            }
        }

    }
}
