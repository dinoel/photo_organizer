﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WpfApplication2
{
    public static class RecursiveIterator
    {
        public static IEnumerable<T> Iterate<T>(Func<T, IEnumerable<T>> sourceIterator, Func<T, bool> filter, T value)
        {
            foreach (var element in sourceIterator(value).SelectMany(o => filter(o) ? new T[] { o } : Iterate(sourceIterator, filter, o)))
            {
                yield return element;
            }
        }
    }
}
