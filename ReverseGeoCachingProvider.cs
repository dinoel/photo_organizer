﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WpfApplication2
{
    public class ReverseGeoCachingProvider
    {
        static string baseUri = "http://maps.googleapis.com/maps/api/" +
                                "geocode/xml?latlng={0},{1}";

        private Dictionary<GeoCoordinate, string> cache = new Dictionary<GeoCoordinate, string>();

        public async Task<string> GetFormatedAddress(GeoCoordinate coordinate)
        {
            if (coordinate == GeoCoordinate.Unknown)
            {
                return "No Address Found";
            }

            string requestUri = string.Format(baseUri, coordinate.Latitude, coordinate.Longitude);

            var item = cache.FirstOrDefault(o => Math.Abs(o.Key.GetDistanceTo(coordinate)) < 500).Value;

            if (item != null)
            {
                return item;
            }

            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                string result = await wc.DownloadStringTaskAsync(requestUri);
                var xmlElm = XElement.Parse(result);

                var status = (from elm in xmlElm.Descendants()
                              where elm.Name == "status"
                              select elm).FirstOrDefault();
                if (status.Value.ToLower() == "ok")
                {
                    var res = (from elm in xmlElm.Descendants()
                               where elm.Name == "formatted_address"
                               select elm).FirstOrDefault();
                    cache[coordinate] = res.Value;
                    return res.Value;
                }
                else
                {
                    return "No Address Found";
                }

            }
        }
    }
}
