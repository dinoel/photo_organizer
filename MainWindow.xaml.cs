﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PictureLocationExtractor pictureLocationExtracter = null;
        private string basePath = @"F:\output\";

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, SymbolicLink dwFlags);

        enum SymbolicLink
        {
            File = 0,
            Directory = 1
        }

        public MainWindow()
        {
            this.pictureLocationExtracter = new PictureLocationExtractor(new ReverseGeoCachingProvider());
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            string path = @"F:\iphone - photos\";
            var files = Directory.GetFileSystemEntries(path).RecursiveSelect(o => Directory.Exists(o) ? Directory.GetFileSystemEntries(o) : null);

            var t = new Thread(() =>
            {
                foreach (var fileInfo in files.AsParallel().Select(file => new FileInfo(file)).Where(file => (file.Attributes & FileAttributes.Directory) != FileAttributes.Directory))
                {
                    CreateLinkToFile(fileInfo);
                };
            });
            t.Start();
        }

        private void CreateLinkToFile(FileInfo fileInfo)
        {
            string location = this.pictureLocationExtracter.GetPictureLocation(fileInfo);
            string destinationDirectory = System.IO.Path.Combine(this.basePath, fileInfo.LastWriteTime.Year.ToString(), fileInfo.LastWriteTime.Month.ToString(), location);
            if (!Directory.Exists(destinationDirectory))
            {
                Directory.CreateDirectory(destinationDirectory);
            }
            string destinationFileName = System.IO.Path.Combine(destinationDirectory, fileInfo.Name);
            if (!File.Exists(destinationFileName))
            {
                CreateSymbolicLink(destinationFileName, fileInfo.FullName, SymbolicLink.File);
                if (Marshal.GetLastWin32Error() != 0)
                {
                    throw new Exception("something went wrong");
                };
            }
            this.Dispatcher.Invoke(() =>
            {
                listBox.Items.Add(fileInfo.FullName);
            });
        }
    }

    public interface IPictureCopier
    {
        FileInfo Copy(FileInfo source, GeoCoordinate coordinate);
    }

    public interface IPictureSorter
    {
        void Sort(FileInfo picture, GeoCoordinate coordinate);
    }
}
