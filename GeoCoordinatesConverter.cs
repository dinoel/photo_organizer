﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
    public class GeoCoordinatesConverter
    {
        public static GeoCoordinate Convert(IIMageGpsInfo imageGpsInfo)
        {

            if (imageGpsInfo.GPSLatitude == null || imageGpsInfo.GPSLatitudeRef == null || 
                imageGpsInfo.GPSLongitude == null || imageGpsInfo.GPSLongitudeRef == null ||
                imageGpsInfo.GPSLatitude.Length != 3 || imageGpsInfo.GPSLongitude.Length != 3)
            {
                return GeoCoordinate.Unknown;
            }

            return new GeoCoordinate
            {
                Latitude = ExifGpsToFloat(imageGpsInfo.GPSLatitudeRef, imageGpsInfo.GPSLatitude),
                Longitude = ExifGpsToFloat(imageGpsInfo.GPSLongitudeRef, imageGpsInfo.GPSLongitude),
            };
        }

        private static double ExifGpsToFloat(string gpsRef, double[] gpsValue)
        {
            double degrees = gpsValue[0];
            double minutes = gpsValue[1];
            double seconds = gpsValue[2];

            double coorditate = degrees + (minutes / 60f) + (seconds / 3600f);

            if (gpsRef == "S" || gpsRef == "W")
                coorditate = 0 - coorditate;
            return coorditate;
        }

    }
}
